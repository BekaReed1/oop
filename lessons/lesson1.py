# import main
a = 'rtyuh'
n = 11
#
# print(main.sum(a,n))

print(type(n))


class Car:
    # свойства
    motor = True

    # метод-def func
    # маг метод инит
    def __init__(self, model, obem, kpd):
        self.model = model
        self.obem = obem
        self.kpd = kpd

    def run(self):
        print(f'run in {self.model}')

    def __str__(self):
        return f'##################\n' \
               f'name is {self.model}\n' \
               f'obem in {self.obem}\n' \
               f'kpd = {self.kpd}\n' \
               f'moter = {self.motor}\n' \
               f'###################'


model1 = Car('tesla', 3.2, 'avtomat')
print(type(model1))
model1.run()
print(model1)
print(model1.motor)
# print(len(model1))
model2=Car('bmw',10,'avtomat')
print(model2)