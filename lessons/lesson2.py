# принципы ооп
# наследование полиморфизм

# инкапсуляция - 2 функции
# DRY
class Bank:  # Суперкласс
    credit = True

    def __init__(self, name, maney, key):
        self.name = name
        self.money = maney
        self.key = key

    def __str__(self):
        return f"name is {self.name}\n" \
               f"деньги - {self.money}"

    def keys(self):
        print(self.key)


object1 = Bank('Бекжан', 1_000_000, '12wetyhterd214f')


# print(object1)
# object1.keys()


class Bank2(Bank):  # дочерний класс
    def __init__(self, name, maney, key, b_date):
        super().__init__(name, maney, key)
        self.date = b_date

    def keys(self):
        print(f'ключ юзера {self.name}'
              f'={self.key}')

    def keys2(self):
        super().keys()

    def __str__(self):
        return f"{super().__str__()}\n" \
               f" {self.date}"


object2 = Bank2('beka', 1_000_00, '1234y5jrytbrgef3', '06-06-2003')
# object2.keys2()

print(object2)





class Bank3(Bank2):
    def __init__(self, name, maney, key, b_date, age):
        Bank2.__init__(self, name, maney, key, b_date)
        self.age = age

    def __str__(self):
        return f"{super().__str__()} \n" \
               f"{self.age}"


object3 = Bank3('Тахир', 1_000_000, '213tef2r13', '21-02-07', 16)
print(object3)
object3.keys()
object3.keys2()



print('#########')
Bank.keys(object2)
