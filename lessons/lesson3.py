# инкапсуляция
# 3 - публичный,_защищенный,__скрытый
class Bank:  # Суперкласс
    credit = True

    def __init__(self, name, maney, key):
        self.name = name
        self.money = maney
        self.__key = key

    def __str__(self):
        return f"name is {self.name}\n" \
               f"деньги - {self.money}"

    def _keys(self):
        print(self.__key)

    def kyus(self):
        self._keys()
object11 = Bank('name', 100000000, 'egvdhhbcedsvc')
object11.name = 'Beka'
object11.money = 0


object11.kyus()
object11._Bank__key = '123edcf7tfgy'
print(object11.__dir__())
